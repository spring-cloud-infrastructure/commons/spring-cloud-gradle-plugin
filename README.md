# Spring Cloud Gradle Plugin

Provides dependency management and common extension configuration for Spring Boot projects within Spring Cloud
Architecture

## Provided configuration

### Plugins management

- `org.springframework.boot` = `2.4.4`
- `io.spring.dependency-management` = `1.0.11.RELEASE`
- `com.pasam.gradle.buildinfo` = `0.1.3`

### Dependency management

- `org.springframework.cloud:spring-cloud-dependencies` = `2020.0.2`

### Build docker image

- `BP_DEBUG_ENABLED` = `true`

### Other configuration

- use `useJUnitPlatform` by default

## 1. Install to local Maven repository

```shell
./gradlew clean publishToMavenLocal
```

## 2. Use in a project

### Groovy DSL

Apply plugin in `build.gradle` file:

```groovy
plugins {
    id 'spring-cloud-gradle-plugin' version '<SPRING_CLOUD_GRADLE_PLUGIN_VERSION>'
}
```

### Kotlin DSL

Apply plugin in `build.gradle.kts` file:

```kotlin
plugins {
    id("spring-cloud-gradle-plugin") version "<SPRING_CLOUD_GRADLE_PLUGIN_VERSION>"
}
```

## 3. Tasks

### My Task

You may use the `--message` parameter to specify the message argument:

```shell
./gradlew myTask --message="What's up y'all"
```

## 4. Extensions

### My Extension

Change configuration of plugin in `build.gradle` file:

```groovy
myExtension {
    message = "Hello"
}
```

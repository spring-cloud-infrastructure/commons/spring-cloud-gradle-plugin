package com.spring.cloud;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.TaskAction;
import org.gradle.api.tasks.options.Option;

import static java.lang.String.format;

public class SpringCloudGradleTask extends DefaultTask {

  @Input
  @Optional
  @Option(description = "Message from command line", option = "message")
  private String message;

  @TaskAction
  public void myTask() {
    final SpringCloudGradleExtension myExtension = (SpringCloudGradleExtension) getProject()
        .getExtensions()
        .findByName("myExtension");
    System.out.println(format("Hello from extension: [%s]", myExtension.getMessage()));
    System.out.println(format("Hello from task: [%s]", message));
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(final String message) {
    this.message = message;
  }
}

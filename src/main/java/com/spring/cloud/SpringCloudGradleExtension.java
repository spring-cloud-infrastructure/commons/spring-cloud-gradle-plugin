package com.spring.cloud;

public class SpringCloudGradleExtension {

  private String message;

  public String getMessage() {
    return message;
  }

  public void setMessage(final String message) {
    this.message = message;
  }
}

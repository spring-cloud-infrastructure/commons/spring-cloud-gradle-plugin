package com.spring.cloud;

import org.gradle.api.Plugin;
import org.gradle.api.Project;

public class SpringCloudGradlePlugin implements Plugin<Project> {

  private static final String MY_EXTENSION_NAME = "myExtension";
  private static final String MY_EXTENSION_DEFAULT_MESSAGE = "default message";
  private static final String MY_TASK_NAME = "myTask";
  private static final String MY_TASK_GROUP = "Spring Cloud";
  private static final String MY_TASK_DESCRIPTION = "Prints message from myExtension as well as message from message argument";
  private static final String MY_TASK_DEFAULT_MESSAGE = "default message";

  @Override
  public void apply(final Project project) {
    final SpringCloudGradleExtension myExtension = project.getExtensions()
        .create(MY_EXTENSION_NAME, SpringCloudGradleExtension.class);
    myExtension.setMessage(MY_EXTENSION_DEFAULT_MESSAGE);

    final SpringCloudGradleTask myTask = project.getTasks()
        .create(MY_TASK_NAME, SpringCloudGradleTask.class);
    myTask.setGroup(MY_TASK_GROUP);
    myTask.setDescription(MY_TASK_DESCRIPTION);
    myTask.setMessage(MY_TASK_DEFAULT_MESSAGE);
  }
}

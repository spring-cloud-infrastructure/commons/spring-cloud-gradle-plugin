package com.spring.cloud;

import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SpringCloudGradlePluginTest {

  private static final String SPRING_CLOUD_GRADLE_PLUGIN_NAME = "spring-cloud-gradle-plugin";
  private static final String MY_EXTENSION_NAME = "myExtension";
  private static final String MY_EXTENSION_DEFAULT_MESSAGE = "default message";
  private static final String MY_TASK_NAME = "myTask";
  private static final String MY_TASK_GROUP = "Spring Cloud";
  private static final String MY_TASK_DESCRIPTION = "Prints message from myExtension as well as message from message argument";
  private static final String MY_TASK_DEFAULT_MESSAGE = "default message";

  private Project pluginProject;

  @BeforeEach
  void setUp() {
    pluginProject = ProjectBuilder.builder().build();
    pluginProject.getPluginManager().apply(SPRING_CLOUD_GRADLE_PLUGIN_NAME);
  }

  @Test
  void testMyExtension() {
    final Object myExtension = pluginProject.getExtensions().findByName(MY_EXTENSION_NAME);

    assertThat(myExtension).isNotNull();
    assertThat(myExtension).isInstanceOf(SpringCloudGradleExtension.class);
    final SpringCloudGradleExtension springCloudGradleExtension = (SpringCloudGradleExtension) myExtension;
    assertThat(springCloudGradleExtension.getMessage()).isEqualTo(MY_EXTENSION_DEFAULT_MESSAGE);
  }

  @Test
  void testMyTask() {
    final Task myTask = pluginProject.getTasks().findByName(MY_TASK_NAME);

    assertThat(myTask).isNotNull();
    assertThat(myTask).isInstanceOf(SpringCloudGradleTask.class);
    final SpringCloudGradleTask springCloudGradleTask = (SpringCloudGradleTask) myTask;
    assertThat(springCloudGradleTask.getGroup()).isEqualTo(MY_TASK_GROUP);
    assertThat(springCloudGradleTask.getDescription()).isEqualTo(MY_TASK_DESCRIPTION);
    assertThat(springCloudGradleTask.getMessage()).isEqualTo(MY_TASK_DEFAULT_MESSAGE);
  }
}
